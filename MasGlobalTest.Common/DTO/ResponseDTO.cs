﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasGlobalTest.Common.DTO
{
    public class ResponseDTO<T>
        where T : new()
    {
        #region Properties
        public HeaderModel Header { get; set; }
        public T Data { get; set; }
        #endregion

        #region Constructor
        public ResponseDTO()
        {
            Header = new HeaderModel();
            Data = new T();
        }
        #endregion
    }

    public class HeaderModel
    {
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
