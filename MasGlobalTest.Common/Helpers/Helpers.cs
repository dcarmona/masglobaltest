﻿using MasGlobalTest.Common.DTO;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace MasGlobalTest.Common.Helpers
{
    public static class Helpers
    {
        public static HeaderModel SetHeader(HttpStatusCode code, string message) 
        {
            return new HeaderModel { Code = (int)code, Message = message };
        
        }
    }
}
