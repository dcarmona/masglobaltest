﻿using MasGlobalTest.Common.Resources;
using MasGlobalTest.Infrastructure.Contracts;
using MasGlobalTest.Infrastructure.Repositories.Interfaces;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace MasGlobalTest.Infrastructure.Repositories.Classes
{
    public class WSEmployeeRepository : IWSEmployeeRepository
    {
        #region Properties
        private readonly IConfiguration _configuration;
        #endregion

        #region Constructor
        public WSEmployeeRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Returns the List of Employees found in EmployeeRepository
        /// </summary>
        /// <param name="condition">predicate to filter within the list</param>
        /// <returns></returns>
        async Task<List<EmployeeContract>> IWSEmployeeRepository.FindAllBy([Optional] Func<EmployeeContract, bool> condition)
        {
            List<EmployeeContract> employeeContracts = new List<EmployeeContract>();
            try
            {
                List<EmployeeContract> responseModelRemote = new List<EmployeeContract>();
                using (HttpClient _client = new HttpClient())
                {
                    string url = _configuration.GetSection("EmployeeRepository")["Url"];
                    string requestUri = _configuration.GetSection("EmployeeRepository")["RequestUri"];
                    string mediaType = _configuration.GetSection("EmployeeRepository")["MediaType"];

                    _client.BaseAddress = new Uri(url);
                    _client.DefaultRequestHeaders.Accept.Clear();
                    _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(mediaType));

                    //Llamado al servicio de EmailAdministrator
                    HttpResponseMessage response = await _client.GetAsync(requestUri);

                    //Valida la respuesta
                    if (response != null && response.IsSuccessStatusCode)
                    {
                        string data = await response.Content.ReadAsStringAsync();
                        if (!string.IsNullOrEmpty(data))
                        {
                            employeeContracts = JsonConvert.DeserializeObject<List<EmployeeContract>>(data);
                            if (employeeContracts != null)
                            {
                                if (condition != null)
                                {
                                    employeeContracts = responseModelRemote.Where(condition).ToList();
                                }
                            }
                        }
                    }
                    else
                    {
                        throw new Exception(ResponseMessages.WSEmployeeRepositoryError);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return employeeContracts;
        }

        /// <summary>
        /// Returns the Employee found in EmployeeRepository with the condition
        /// </summary>
        /// <param name="condition">predicate to filter the employee</param>
        /// <returns></returns>
        async Task<EmployeeContract> IWSEmployeeRepository.FindSingleBy(Func<EmployeeContract, bool> condition)
        {
            EmployeeContract employeeContract = new EmployeeContract();
            try
            {
                List<EmployeeContract> responseModelRemote = new List<EmployeeContract>();
                using (HttpClient _client = new HttpClient())
                {
                    string url = _configuration.GetSection("EmployeeRepository")["Url"];
                    string requestUri = _configuration.GetSection("EmployeeRepository")["RequestUri"];
                    string mediaType = _configuration.GetSection("EmployeeRepository")["MediaType"];

                    _client.BaseAddress = new Uri(url);
                    _client.DefaultRequestHeaders.Accept.Clear();
                    _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(mediaType));

                    //Llamado al servicio de EmailAdministrator
                    HttpResponseMessage response = await _client.GetAsync(requestUri);

                    //Valida la respuesta
                    if (response != null && response.IsSuccessStatusCode)
                    {
                        string data = await response.Content.ReadAsStringAsync();
                        if (!string.IsNullOrEmpty(data))
                        {
                            responseModelRemote = JsonConvert.DeserializeObject<List<EmployeeContract>>(data);
                            if (responseModelRemote != null)
                            {
                                employeeContract = responseModelRemote.Where(condition).FirstOrDefault();
                            }
                        }
                    }
                    else
                    {
                        throw new Exception(ResponseMessages.WSEmployeeRepositoryError);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return employeeContract;
        }
        #endregion

        #region Private Methods

        #endregion
    }
}
