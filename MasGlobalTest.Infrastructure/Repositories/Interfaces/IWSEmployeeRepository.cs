﻿using MasGlobalTest.Infrastructure.Contracts;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace MasGlobalTest.Infrastructure.Repositories.Interfaces
{
    public interface IWSEmployeeRepository
    {
        Task<EmployeeContract> FindSingleBy(Func<EmployeeContract, bool> condition);
        Task<List<EmployeeContract>> FindAllBy([Optional] Func<EmployeeContract, bool> condition);
    }
}
