﻿using MasGlobalTest.Domain.Models;
using MasGlobalTest.Domain.Services.Classes;
using System;
using System.Collections.Generic;
using System.Text;
using MasGlobalTest.Domain.Classes.SalaryFactoryMethod;
using Xunit;

namespace MasGlobalTest.Test.SalaryTests
{
    public class SalaryTests
    {
        [Fact]
        public void CalculateEmployeeSalary_HourlySalary_ReturnsDouble() 
        {
            // Arrange
            var employeeService = new EmployeeService(null, null);
            var employeeModel = new EmployeeModel
            {
                Id = 1,
                Name = "Andrea",
                ContractTypeName = "HourlySalaryEmployee",
                RoleId = 1,
                RoleName = "Administrator",
                RoleDescription = null,
                HourlySalary = 10000,
                MonthlySalary = 50000
            };
            // Act
            double actual = employeeService.CalculateEmployeeSalary(employeeModel);
            double expected = 120 * employeeModel.HourlySalary * 12;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateEmployeeSalary_MonthlySalary_ReturnsDouble()
        {
            // Arrange
            var employeeService = new EmployeeService(null, null);
            var employeeModel = new EmployeeModel
            {
                Id = 1,
                Name = "Andrea",
                ContractTypeName = "MonthlySalaryEmployee",
                RoleId = 1,
                RoleName = "Administrator",
                RoleDescription = null,
                HourlySalary = 10000,
                MonthlySalary = 50000
            };
            // Act
            double actual = employeeService.CalculateEmployeeSalary(employeeModel);
            double expected = employeeModel.MonthlySalary * 12;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateEmployeeSalary_NoSalary_ReturnsZero()
        {
            // Arrange
            var employeeService = new EmployeeService(null, null);
            var employeeModel = new EmployeeModel
            {
                Id = 1,
                Name = "Andrea",
                ContractTypeName = "",
                RoleId = 1,
                RoleName = "Administrator",
                RoleDescription = null,
                HourlySalary = 10000,
                MonthlySalary = 50000
            };
            // Act
            // double actual = EmployeeService.CalculateEmployeeSalary(employeeModel);
            // Assert
            Assert.Throws<InvalidOperationException>(
                () => employeeService.CalculateEmployeeSalary(employeeModel)
                );
        }

        [Fact]
        public void CalculateEmployeeSalary_MonthlyNoRateSalary_ThrowsException()
        {
            // Arrange
            var employeeService = new EmployeeService(null, null);
            var employeeModel = new EmployeeModel
            {
                ContractTypeName = "MonthlySalaryEmployee",
            };
            // Act
            
            // Assert
            Assert.Throws<ArgumentException>(
                () => employeeService.CalculateEmployeeSalary(employeeModel));
        }

        [Fact]
        public void CalculateEmployeeSalary_HourlyNoRateSalary_ThrowsException()
        {
            // Arrange
            var employeeService = new EmployeeService(null, null);
            var employeeModel = new EmployeeModel
            {
                ContractTypeName = "HourlySalaryEmployee",
            };
            // Act

            // Assert
            Assert.Throws<ArgumentException>(
                () => employeeService.CalculateEmployeeSalary(employeeModel));
        }
    }
}
