import { Component } from '@angular/core';
import { EmployeeService } from '../services/employee/employee.service';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { EmployeeModel } from '../models/employee';
import { ResponseModel } from '../models/response';
import { AlerthttpService } from '../services/alerthttp/alerthttp.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent {
  
  form: FormGroup = new FormGroup({});
  public employees: EmployeeModel[] = [];
  constructor(private employeeService: EmployeeService, private fb: FormBuilder, private httpAlertService: AlerthttpService) {
    this.form = fb.group({
      id: ['', [Validators.pattern("^[1-9][0-9]*$")]]
    })
  }

  get f() {
    return this.form.controls;
  }

  async submit() {
    if(this.form.value.id > 0){
      // Get by id
      await this.GetEmployeeById(this.form.value.id);
    }else{
      await this.GetEmployeeAll();
    }
  }

  private async GetEmployeeById(id:number): Promise<void> {
    const response: ResponseModel<EmployeeModel> = await this.employeeService.GetEmployeeById(id);
    this.employees = [];
    if(this.httpAlertService.IsSuccessStatusCode(response.header.code)){
      this.employees[0] = response.data;
    }

    this.httpAlertService.Handler(response.header.code, response.header.message);
  }

  private async GetEmployeeAll(): Promise<void> {
    const response: ResponseModel<EmployeeModel[]> = await this.employeeService.GetAllEmployee();
    this.employees = [];
    if(this.httpAlertService.IsSuccessStatusCode(response.header.code)){
      this.employees = response.data;
    }

    this.httpAlertService.Handler(response.header.code, response.header.message);
  }

}
