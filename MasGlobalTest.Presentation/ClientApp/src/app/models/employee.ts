export class EmployeeModel{
    public Id: number ;
    public Name: string ;
    public ContractTypeName: string ;
    public RoleName: string ;
    public RoleDescription: string ;
    public Salary: number ;
}