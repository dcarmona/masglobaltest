export class ResponseModel<T>{
    public header: HeaderModel;
    public data: T;
}

export class HeaderModel{
    public code: number;
    public message: string;
}