import { Inject, Injectable } from '@angular/core';
import { Urls } from 'src/app/models/urls';

@Injectable({
  providedIn: 'root'
})
export class ConstantsService {
  BASE_URL: string;
  API_VERSION: string;
  public URLS: Urls = {
    GetEmployeeById: (Id: number): string => `${this.BASE_URL}api/${this.API_VERSION}/Employees/${Id}`,
    GetEmployeeAll: (): string => `${this.BASE_URL}api/${this.API_VERSION}/Employees`,
  };
  constructor(@Inject('BASE_URL') baseUrl: string, @Inject('API_VERSION') apiVersion: string) {
    this.BASE_URL = baseUrl;
    this.API_VERSION = apiVersion;
  }
}
