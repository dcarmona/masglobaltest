import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeModel } from 'src/app/models/employee';
import { ResponseModel } from 'src/app/models/response';
import { ConstantsService } from '../common/constants.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient, private constantService: ConstantsService, private router: Router) { }

  public GetEmployeeById(id: number) : Promise<ResponseModel<EmployeeModel>>{
    return this.http.get<ResponseModel<EmployeeModel>>(this.constantService.URLS.GetEmployeeById(id)).toPromise();;
  }

  public GetAllEmployee() : Promise<ResponseModel<EmployeeModel[]>>{
    return this.http.get<ResponseModel<EmployeeModel[]>>(this.constantService.URLS.GetEmployeeAll()).toPromise();;
  }
}
