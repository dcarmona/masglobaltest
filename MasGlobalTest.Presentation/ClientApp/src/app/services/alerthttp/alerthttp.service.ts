import { Injectable } from '@angular/core';
import { AlertService } from 'src/app/_alert';

@Injectable({
  providedIn: 'root'
})
export class AlerthttpService {
  options = {
    autoClose: true,
    keepAfterRouteChange: true
  };
  constructor(private alertService: AlertService) { }

  private Ok(message: string) {
    this.alertService.success(message, this.options);
  }

  private Redirect(message: string) {
    this.alertService.warn(message, this.options);
  }

  private ClientError(message: string) {
    this.alertService.error(message, this.options);
  }

  private ServerError(message: string) {
    this.alertService.error(message, this.options);
  }

  /**
   * IsSuccessStatusCode
   */
  public IsSuccessStatusCode(httpCode: number): boolean{
    if(httpCode == 200){
      return true;
    }
    return false;
  }

  /**
   * Handler
   */
  public Handler(httpCode: number, message: string) {
    if (httpCode > 200 && httpCode < 300) {
      this.Ok(message)
    }else if (httpCode >= 300 && httpCode < 400) {
      this.Redirect(message);
    } else if (httpCode >= 400 && httpCode < 500) {
      this.ClientError(message);
    } else if (httpCode >= 500){
      this.ServerError(message);
    } 
  }
}
