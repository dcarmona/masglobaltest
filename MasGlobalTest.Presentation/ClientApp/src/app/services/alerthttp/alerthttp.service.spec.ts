import { TestBed } from '@angular/core/testing';

import { AlerthttpService } from './alerthttp.service';

describe('AlerthttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AlerthttpService = TestBed.get(AlerthttpService);
    expect(service).toBeTruthy();
  });
});
