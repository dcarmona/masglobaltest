﻿using MasGlobalTest.Common.DTO;
using MasGlobalTest.Domain.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MasGlobalTest.WebApi.Controllers
{
    /// <summary>
    /// Employee Controller - Handles the operations related with it
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        #region Properties
        private readonly IEmployeeService _employeeService;
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeService"></param>
        public EmployeesController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Returns the list of all Employees
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseDTO<List<EmployeeDTO>>> Get()
            => await _employeeService.FindAll();

        /// <summary>
        /// Returns a single object of Employee
        /// </summary>
        /// <param name="id">Id of the employee</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ResponseDTO<EmployeeDTO>> Get(string id) 
            => await _employeeService.FindById(id);
        #endregion
    }
}
