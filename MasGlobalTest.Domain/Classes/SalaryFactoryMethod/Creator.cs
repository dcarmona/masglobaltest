﻿using MasGlobalTest.Domain.Classes.SalaryFactoryMethod.Concretes;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasGlobalTest.Domain.Classes.SalaryFactoryMethod
{
    abstract class Creator
    {
        public abstract ISalary FactoryMethod();

        public double CalculateSalary()
        {
            var contractType = FactoryMethod();
            if (contractType != null)
            {
                return contractType.Calculate();
            }

            throw new InvalidOperationException("Contract Type cannot be null");
        }
    }
}
