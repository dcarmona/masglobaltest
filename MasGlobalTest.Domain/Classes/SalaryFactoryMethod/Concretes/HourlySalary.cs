﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasGlobalTest.Domain.Classes.SalaryFactoryMethod.Concretes
{
    class HourlySalary : ISalary
    {
        private readonly double hourlySalary;

        public HourlySalary(double hourlySalary)
        {
            this.hourlySalary = hourlySalary;
        }

        public double Calculate()
        {
            if (hourlySalary > 0)
            {
                return 120 * hourlySalary * 12;
            }

            throw new ArgumentException("Hourly Salary cannot be Zero");
        }
    }
}
