﻿namespace MasGlobalTest.Domain.Classes.SalaryFactoryMethod.Concretes
{
    public interface ISalary
    {
        double Calculate();
    }
}