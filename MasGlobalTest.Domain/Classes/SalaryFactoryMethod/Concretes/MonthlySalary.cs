﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasGlobalTest.Domain.Classes.SalaryFactoryMethod.Concretes
{
    class MonthlySalary : ISalary
    {
        private readonly double monthlySalary;

        public MonthlySalary(double monthlySalary)
        {
            this.monthlySalary = monthlySalary;
        }

        public double Calculate()
        {
            if (monthlySalary > 0)
            {
                return monthlySalary * 12;
            }
            
            throw new ArgumentException("Monthly Salary cannot be Zero");
        }
    }
}
