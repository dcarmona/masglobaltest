﻿using MasGlobalTest.Domain.Classes.SalaryFactoryMethod.Concretes;
using MasGlobalTest.Domain.Enums;
using MasGlobalTest.Domain.Models;

namespace MasGlobalTest.Domain.Classes.SalaryFactoryMethod
{
    class ConcreteCreatorContractType : Creator
    {
        private readonly EmployeeModel employee;

        public ConcreteCreatorContractType(EmployeeModel employee)
        {
            this.employee = employee;
        }

        public override ISalary FactoryMethod()
        {
            ISalary concrete = null;
            switch (employee.ContractTypeName)
            {
                case nameof(ContractType.MonthlySalaryEmployee):
                    return new MonthlySalary(employee.MonthlySalary);
                case nameof(ContractType.HourlySalaryEmployee):
                    return new HourlySalary(employee.HourlySalary);
                default:
                    break;
            }

            return concrete;
        }
    }
}
