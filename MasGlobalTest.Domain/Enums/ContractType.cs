﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasGlobalTest.Domain.Enums
{
    enum ContractType 
    {
        MonthlySalaryEmployee,
        HourlySalaryEmployee
    }
}
