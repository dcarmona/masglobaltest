﻿using AutoMapper;
using MasGlobalTest.Common.DTO;
using MasGlobalTest.Common.Helpers;
using MasGlobalTest.Common.Resources;
using MasGlobalTest.Domain.Classes.SalaryFactoryMethod;
using MasGlobalTest.Domain.Models;
using MasGlobalTest.Domain.Services.Interfaces;
using MasGlobalTest.Infrastructure.Contracts;
using MasGlobalTest.Infrastructure.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MasGlobalTest.Domain.Services.Classes
{
    public class EmployeeService : IEmployeeService
    {
        #region Properties
        private readonly IWSEmployeeRepository _wsEmployeeRepository;
        private readonly IMapper _mapper;
        #endregion

        #region Constructor
        public EmployeeService(IWSEmployeeRepository wsEmployeeRepository, IMapper mapper)
        {
            _wsEmployeeRepository = wsEmployeeRepository;
            _mapper = mapper;
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Returns the list of Employees
        /// </summary>
        /// <returns></returns>
        async Task<ResponseDTO<List<EmployeeDTO>>> IEmployeeService.FindAll()
        {
            ResponseDTO<List<EmployeeDTO>> response = new ResponseDTO<List<EmployeeDTO>>
            {
                Header = Helpers.SetHeader(HttpStatusCode.OK, HttpStatusCode.OK.ToString())
            };
            try
            {
                List<EmployeeContract> employeeContracts = await _wsEmployeeRepository.FindAllBy();
                if (employeeContracts == null || employeeContracts.Count == 0)
                {
                    response.Header = Helpers.SetHeader(HttpStatusCode.NoContent, ResponseMessages.EmployeesNoData);
                    return response;
                }

                List<EmployeeModel> employeeModels = _mapper.Map<List<EmployeeModel>>(employeeContracts);

                foreach (EmployeeModel employeeModel in employeeModels)
                {
                    EmployeeDTO employee = _mapper.Map<EmployeeDTO>(employeeModel);

                    // Get Salary
                    employee.Salary = CalculateEmployeeSalary(employeeModel);

                    response.Data.Add(employee);
                }

            }
            catch (Exception ex)
            {
                response.Header = Helpers.SetHeader(HttpStatusCode.InternalServerError, ex.Message);
            }
            return response;
        }

        /// <summary>
        /// Returns the Employee given an ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        async Task<ResponseDTO<EmployeeDTO>> IEmployeeService.FindById(string id)
        {
            ResponseDTO<EmployeeDTO> response = new ResponseDTO<EmployeeDTO>
            {
                Header = Helpers.SetHeader(HttpStatusCode.OK, HttpStatusCode.OK.ToString())
            };
            try
            {
                if (int.TryParse(id, out int userId))
                {
                    EmployeeModel employeeModel = _mapper.Map<EmployeeModel>(await _wsEmployeeRepository.FindSingleBy(x => x.Id == userId));
                    if (employeeModel == null)
                    {
                        response.Header = Helpers.SetHeader(HttpStatusCode.BadRequest, ResponseMessages.EmployeeNotFound);
                        return response;
                    }

                    EmployeeDTO employee = _mapper.Map<EmployeeDTO>(employeeModel);

                    // Get Salary
                    employee.Salary = CalculateEmployeeSalary(employeeModel);

                    response.Data = employee;
                }
                else
                {
                    response.Header = Helpers.SetHeader(HttpStatusCode.BadRequest, ResponseMessages.EmployeeByIdBadParameter);
                }
            }
            catch (Exception ex)
            {
                response.Header = Helpers.SetHeader(HttpStatusCode.InternalServerError, ex.Message);
            }

            return response;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Using factory method calculates the salary of the given employee, ContractTypeName is the 
        /// indicator to calculate 
        /// </summary>
        /// <param name="employeeModel"></param>
        /// <returns></returns>
        public double CalculateEmployeeSalary(EmployeeModel employeeModel)
        {
            // Get Contract type
            ConcreteCreatorContractType contractType = new ConcreteCreatorContractType(employeeModel);
            return contractType.CalculateSalary();
        }
        #endregion
    }
}
