﻿using MasGlobalTest.Common.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MasGlobalTest.Domain.Services.Interfaces
{
    public interface IEmployeeService
    {
        Task<ResponseDTO<EmployeeDTO>> FindById(string id);
        Task<ResponseDTO<List<EmployeeDTO>>> FindAll();
    }
}
