﻿using MasGlobalTest.Domain.Services.Classes;
using MasGlobalTest.Domain.Services.Interfaces;
using MasGlobalTest.Infrastructure.Repositories.Classes;
using MasGlobalTest.Infrastructure.Repositories.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.IO;

namespace MasGlobalTest.Configuration
{
    public class ContainerConfig
    {
        public static void Register(IServiceCollection services, IConfiguration configuration)
        {
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddApiVersioning();

            //IConfiguration
            services.AddSingleton(provider => configuration);

            //Add dependencies
            services.AddTransient<IWSEmployeeRepository, WSEmployeeRepository>();
            services.AddTransient<IEmployeeService, EmployeeService>();

            //Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "EmployeesApi" });

                var xmlPath = Path.Combine(Environment.CurrentDirectory, "WebApi.xml");
                c.IncludeXmlComments(xmlPath);
                c.DescribeAllParametersInCamelCase();
            });
        }
    }
}
