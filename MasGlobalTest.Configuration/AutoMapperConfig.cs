﻿using AutoMapper;
using MasGlobalTest.Common.DTO;
using MasGlobalTest.Domain.Models;
using MasGlobalTest.Infrastructure.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace MasGlobalTest.Configuration
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            //Contract - Model
            CreateMap<EmployeeModel, EmployeeContract>().ReverseMap();
            CreateMap<EmployeeDTO, EmployeeModel>().ReverseMap();
        }
    }
}
